require 'test_helper'

class TagTest < ActiveSupport::TestCase
  def test_cannot_save_empty_tag
    tag = Tag.new
    refute tag.valid?
    assert tag.errors[:title].any?
  end
  
  def test_cannot_save_duplicate_tag
    tag = Tag.new(title: 'talkative')
    refute tag.valid?
    assert tag.errors[:title].any?
    assert_equal ["has already been taken"], tag.errors[:title]
  end
end