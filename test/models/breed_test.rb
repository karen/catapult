require 'test_helper'

class BreedTest < ActiveSupport::TestCase
  def test_cannot_save_empty_breed
    breed = Breed.new
    refute breed.valid?
    assert breed.errors[:name].any?
  end
  
  def test_cannot_save_breed_with_duplicate_name
    breed = Breed.new(name: 'British Shorthair')
    refute breed.valid?
    assert breed.errors[:name].any?
    assert_equal ["has already been taken"], breed.errors[:name]
  end
end