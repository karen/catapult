require 'test_helper'

class TagSerializerTest < ActiveSupport::TestCase
  test 'includes title, breed_count and breed_ids' do
    response = TagSerializer.new(tags(:one)).as_json
    assert_equal [:id, :title, :breed_count, :breed_ids], response.keys
  end
end