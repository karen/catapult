require 'test_helper'

class BreedSerializerTest < ActiveSupport::TestCase
  test 'includes name, tag_count and tag_ids' do
    response = BreedSerializer.new(breeds(:one)).as_json
    assert_equal [:id, :name, :tag_count, :tag_ids], response.keys
  end
end