require 'test_helper'

class SimpleTagSerializerTest < ActiveSupport::TestCase
  test 'includes title and breeds' do
    response = SimpleTagSerializer.new(tags(:one)).as_json
    assert_equal [:title, :breeds], response.keys
  end
end