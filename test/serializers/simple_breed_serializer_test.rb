require 'test_helper'

class SimpleBreedSerializerTest < ActiveSupport::TestCase
  test 'includes name and tags' do
    response = SimpleBreedSerializer.new(breeds(:one)).as_json
    assert_equal [:name, :tags], response.keys
  end
end