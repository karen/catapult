require 'test_helper'

class TagsControllerTest < ActionController::TestCase
  test 'should get index without breed' do
    get :index
    assert_response :success
  end
  
  test 'should get index with breed id' do
    get :index,
      params: {
        breed_id: breeds(:one).id
      }
    assert_response :success
  end
  
  test 'should get show' do
    get :show,
      params: {
        id: tags(:one).id
      }
    assert_response :success
  end
  
  test 'should update tag' do
    patch :update,
      params: {
        id: tags(:one).id,
        tag: { 
          title: 'Another cat'
        }
      }
    assert_response :success
  end
  
  test 'should destroy tag' do
    assert_difference("Tag.count", -1) do
      delete :destroy,
        params: {
          id: tags(:one).id
        }
    end
    assert_response :success
  end
  
  test 'should destroy tag and all its associations' do
    breed = breeds(:one)
    tag = tags(:one)
    breed.tags << tag
    assert_difference(["Tag.count", "BreedTag.count"], -1) do
      delete :destroy,
        params: {
          id: tags(:one).id
        }
    end
    assert_response :success
  end
end