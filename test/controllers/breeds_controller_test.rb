require 'test_helper'

class BreedsControllerTest < ActionController::TestCase
  test 'should get stats' do
    get :stats
    assert_response :success
  end
  
  test 'should update tags on breed' do
    assert_difference("BreedTag.count") do
      post :tags,
        params: {
          id: breeds(:one).id,
          tags: [
            'loves to swim'
          ]
        }
    end
    assert_response :success
  end
  
  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:breeds)
  end
  
  test 'should get show' do
    get :show, 
      params: {
        id: breeds(:one)
      }
    assert_response :success
    assert_equal ['name', 'tags'], JSON.parse(response.body).keys
  end
  
  test 'should create breed and tags' do
    assert_difference(["Breed.count", "Tag.count"]) do
      post :create,
        params: {
          breed: {
            name: 'Persian'
          }, 
          tags: [
            'has blue eyes'
          ]
        }
    end
  end
  
  test 'should create breed without tags' do
    assert_no_difference("Tag.count") do
      post :create,
        params: {
          breed: {
            name: 'Persian'
          }
        }
    end
  end
  
  test 'should not create empty breed' do
    assert_no_difference(["Breed.count", "Tag.count"]) do
      post :create,
        params: {
          breed: {
            name: 'British Shorthair'
          }
        },
        tags: [
          'some tag'
        ]
    end
    assert_response 422
    assert_equal ["has already been taken"], JSON.parse(response.body)['errors']['name']
  end
  
  test 'should update breed and remove all tags' do
    breed = breeds(:one)
    patch :update,
      params: {
        id: breed.id,
        breed: {
          name: 'British Shorthair2'
        },
        tags: []
      }
    assert_response :success
    assert breed.tags.empty?
  end
  
  test 'should update breed and add one tag' do
    breed = breeds(:one)
    assert_difference("Tag.count") do
      patch :update,
        params: {
          id: breed.id,
          breed: {
            name: 'British Shorthair'
          },
          tags: [
            'loves to play'
          ]
        }
    end
    assert_response :success
    assert_equal 1, breed.tags.count  
  end
  
  test 'should update breed and add an existing tag' do
    breed = breeds(:one)
    assert_no_difference("Tag.count") do
      patch :update,
        params: {
          id: breed.id,
          breed: {
            name: 'British Shorthair'
          },
          tags: [
            'loves to swim'
          ]
        }
    end
    assert_response :success
    assert_equal 1, breed.tags.count  
  end
  
  test 'should delete breed' do
    breed = breeds(:one)
    assert_difference("Breed.count", -1) do
      delete :destroy,
        params: {
          id: breed.id
        }
    end
    assert_response :success
  end
  
  test 'should delete breed and tag' do
    breed = breeds(:one)
    breed.tags << tags(:one)
    assert_difference(["Breed.count", "Tag.count"], -1) do
      delete :destroy,
        params: {
          id: breed.id
        }
    end
    assert_response :success
  end
  
  test 'should delete breed but not tag' do
    breed = breeds(:one)
    breed2 = breeds(:two)
    tag = tags(:one)
    [breed, breed2].each{ |b| b.tags << tag }
    
    assert_no_difference("Tag.count") do
      delete :destroy,
        params: {
          id: breed.id
        }
    end
    assert_response :success
  end
end