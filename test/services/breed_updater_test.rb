require 'test_helper'

class BreedUpdaterTest < ActiveSupport::TestCase
  test 'initializes all variables' do
    updater = BreedUpdater.new(breeds(:one), { name: 'Scottish Fold' }, ['talkative'])
    assert_not_nil updater.breed
    assert_not_nil updater.breed_params
    assert_not_nil updater.tags_params
  end
  
  test 'updates breed' do
    breed = breeds(:one)
    updater = BreedUpdater.new(breed, { name: 'Scottish Fold' }, ['talkative'])
    assert updater.save
    assert_equal 'Scottish Fold', breed.name
  end
  
  test 'assigns new tag and removes old tag' do
    breed = breeds(:one)
    breed.tags << tags(:one)
    updater = BreedUpdater.new(breed, { name: 'Scottish Fold' }, ['loves to play'])
    assert_difference("Tag.count") do
      updater.save
    end
    assert_equal 1, breed.tags.count
    assert_equal 'loves to play', breed.tags.first.title
  end
  
  test 'assigns existing tag' do
    breed = breeds(:one)
    updater = BreedUpdater.new(breed, { name: 'Scottish Fold' }, [tags(:one).title])
    assert_no_difference("Tag.count") do
      updater.save
    end
    assert_equal tags(:one), breed.tags.first
  end
  
  test 'removes all tags when tags is empty' do
    breed = breeds(:one)
    breed.tags << tags(:one)
    updater = BreedUpdater.new(breed, { name: 'Scottish Fold' })
    updater.save
    assert breed.tags.empty?
  end
end