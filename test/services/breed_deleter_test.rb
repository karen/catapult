require 'test_helper'

class BreedDeleterTest < ActiveSupport::TestCase
  test 'initializes variable' do
    deleter = BreedDeleter.new(breeds(:one))
    assert_not_nil deleter.breed
  end
  
  test 'deletes breed' do
    deleter = BreedDeleter.new(breeds(:one))
    assert_difference("Breed.count", -1) do
      deleter.destroy
    end
  end
  
  test 'deletes orphan tags' do
    breed = breeds(:one)
    breed.tags << tags(:one)
    deleter = BreedDeleter.new(breeds(:one))
    assert_difference(["Breed.count", "Tag.count"], -1) do
      deleter.destroy
    end
  end
  
  test 'does not delete tags associated with other breeds' do
    breed = breeds(:one)
    breed2 = breeds(:two)
    [breed, breed2].each { |b| b.tags << tags(:one) }
    deleter = BreedDeleter.new(breeds(:one))
    assert_no_difference("Tag.count") do
      deleter.destroy
    end
  end
end