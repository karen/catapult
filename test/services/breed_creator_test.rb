require 'test_helper'

class BreedCreatorTest < ActiveSupport::TestCase
  test 'initializes a new breed' do
    creator = BreedCreator.new({name: 'Some breed'})
    assert creator.breed
  end
  
  test 'saves new breed' do
    creator = BreedCreator.new({name: 'Some breed'})
    assert_difference("Breed.count") do
      creator.save
    end
  end
  
  test 'saves new breed and assigns existing tag' do
    creator = BreedCreator.new({name: 'Some breed'}, ['loves to swim'])
    assert_no_difference("Tag.count") do
      creator.save
    end
  end
  
  test 'saves new breed and tag' do
    creator = BreedCreator.new({name: 'Some breed'}, ['loves to play'])
    assert_difference(["Breed.count", "Tag.count"]) do
      creator.save
    end
  end
end