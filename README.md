# README

This application is an implementation of the first step of the Catapult coding challenge.
It consists of a basic cat tagging API.

### Ruby version
2.3.1-p112

### System dependencies
* Ruby 2.3.1
* Rails 5.0.6
* JSON

To test whether you have JSON, follow these steps in a console:
```
require 'json'
 => true
```
If this returns `false`, run `gem install json` in your terminal.

### Configuration
1. Clone the git repo to your machine and cd into the directory `cd catapult`
2. Install gems `bundle install`
3. Run migrations `bundle exec rake db:migrate`
4. Seed the database `bundle exec rake db:seed`

You're ready to go! Start your server `bin/rails server` and go to `http://localhost:3000/breeds`. 
You should see this output:
```
[
  {
    "name":"American Bobtail",
    "tags":[
      "affectionate",
      "low shedding",
      "playful",
      "intelligent"
    ]
  },{
    "name":"Cymric",
    "tags":[
      "affectionate",
      "has no tail",
      "friendly"
    ]
  },{
    "name":"Norwegian Forest Cat",
    "tags":[
      "low shedding",
      "pet friendly",
      "knows kung fu",
      "climbs trees"
    ]
  }
]
```

### Testing
To run the test suite, run `bin/rails test` in your terminal.


## Available Routes
```
      Prefix Verb   URI Pattern                          Controller#Action
stats_breeds GET    /breeds/stats(.:format)          breeds#stats
 tags_breeds POST   /breeds/tags(.:format)           breeds#tags
  breed_tags GET    /breeds/:breed_id/tags(.:format) tags#index
      breeds GET    /breeds(.:format)                breeds#index
             POST   /breeds(.:format)                breeds#create
       breed GET    /breeds/:id(.:format)            breeds#show
             PATCH  /breeds/:id(.:format)            breeds#update
             PUT    /breeds/:id(.:format)            breeds#update
             DELETE /breeds/:id(.:format)            breeds#destroy
  stats_tags GET    /tags/stats(.:format)            tags#stats
        tags GET    /tags(.:format)                  tags#index
         tag GET    /tags/:id(.:format)              tags#show
             PATCH  /tags/:id(.:format)              tags#update
             PUT    /tags/:id(.:format)              tags#update
             DELETE /tags/:id(.:format)              tags#destroy
```

### Rationale
1. I chose to use a `has_many, through` association between `Breed` and `Tag`, and use a `dependent: :destroy` on the join_table `BreedTag`.
2. I chose to use services for the creation, updating and deletion of the `Breed` resource because I needed to perform action concerning `Tag` as well. This keeps the controller clean and makes it easier to test.
3. I chose to use serializers for the serialization of `Breed` and `Tag`, and to also create a 'Simple' serializer to control the json response.
4. I chose to create a separate `post :tags` routes within the `resources :breeds` definition in `routes.rb` because I didn't want the `update` method in `TagsController` to do two things.
5. I chose to use `pry` for debugging because I am more comfortable with it than `byebug`.
6. I chose to create a Rails app using the `rails new catapult --api` argument to avoid creating unneccessary views etc.

### Improvements
1. Separate the actions on `Tag` into services, which are called from the services for `Breed`
2. Add validation to `BreedTag` to ensure unique combinations of `Breed` and `Tag`. The code enforces this implicitly by doing a `find_or_create`, but it can be more explicit.
3. Create QueryObjects for the `/stats` endpoints if the stats become more complex. I feel that right now it is overkill to put the calculation of the counts in a separate QueryObject.
