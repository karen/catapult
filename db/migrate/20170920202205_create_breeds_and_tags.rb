class CreateBreedsAndTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string :title
      t.timestamps
    end
    
    create_table :breeds do |t|
      t.string :name
      t.timestamps
    end
    
    create_table :breed_tags do |t|
      t.belongs_to :breed, index: true
      t.belongs_to :tag, index: true
    end
  end
end
