# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

['affectionate', 'low shedding', 'playful', 'intelligent', 'has no tail', 'friendly', 'pet friendly', 'knows kung fu', 'climbs trees'].each do |trait|
  Tag.create(title: trait)
end

['American Bobtail', 'Cymric', 'Norwegian Forest Cat'].each do |breed|
  Breed.create(name: breed)
end

Breed.find_by(name: 'American Bobtail').tags << Tag.where(title: ['affectionate', 'low shedding', 'playful', 'intelligent'])
Breed.find_by(name: 'Cymric').tags << Tag.where(title: ['affectionate', 'has no tail', 'friendly'])
Breed.find_by(name: 'Norwegian Forest Cat').tags << Tag.where(title: ['low shedding', 'pet friendly', 'knows kung fu', 'climbs trees'])