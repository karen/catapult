Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :breeds do
    collection do
      get :stats
      post :tags
    end
    
    resources :tags, only: [:index]
  end
  
  resources :tags, except: [:create] do
    collection do
      get :stats
    end
  end
    
end
