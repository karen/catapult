class BreedTag < ApplicationRecord
  belongs_to :breed
  belongs_to :tag
  
  validates :breed_id, presence: true
  validates :tag_id, presence: true
end