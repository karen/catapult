class Breed < ApplicationRecord
  has_many :breed_tags, inverse_of: :breed, dependent: :destroy
  has_many :tags, through: :breed_tags
  
  validates :name, presence: true, uniqueness: true
end