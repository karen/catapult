class TagsController < ApplicationController
  before_action :set_tag, only: [:show, :destroy, :update]
  
  def stats
    @tags = Tag.includes(:breeds).all
    
    render json: @tags
  end
  
  def index
    if params[:breed_id]  
      @breed = Breed.find params[:breed_id]
      @tags = @breed.tags
    end
    @tags ||= Tag.all
    
    render json: @tags, each_serializer: SimpleTagSerializer
  end
  
  def update
    if @tag.update(tag_params)
      head :ok
    else
      render json: { errors: @tag.errors }, status: 422
    end
  end
  
  def show
    render json: @tag, serializer: SimpleTagSerializer
  end
  
  def destroy
    if @tag.destroy
      head :ok
    else
      render status: 500, json: { message: 'Could not destroy tag' }
    end
  end
  
  private
  
  def tag_params
    params.require(:tag).permit(:title)
  end
  
  def set_tag
    @tag = Tag.includes(:breeds).find(params[:id])
  end
end