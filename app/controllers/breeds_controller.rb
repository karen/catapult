class BreedsController < ApplicationController
  before_action :set_breed, only: [:show, :update, :destroy, :tags]
  
  def stats
    @breeds = Breed.includes(:tags).all
    
    render json: @breeds
  end
  
  def tags
    @breed_updater = BreedUpdater.new(@breed, nil, params[:tags])
    if @breed_updater.save
      head :ok
    else
      render json: { errors: @breed_creator.breed.errors }, status: 422
    end
  end
  
  def index
    @breeds = Breed.includes(:tags).all
    render json: @breeds, each_serializer: SimpleBreedSerializer
  end
  
  def show
    if @breed
      render json: @breed, serializer: SimpleBreedSerializer
    else
      head :not_found
    end
  end
  
  def create
    @breed_creator = BreedCreator.new(breed_params, params[:tags])
    if @breed_creator.save
      render json: @breed_creator.breed
    else
      render json: { errors: @breed_creator.breed.errors }, status: 422
    end
  end
  
  def update
    @breed_updater = BreedUpdater.new(@breed, breed_params, params[:tags])
    if @breed_updater.save
      head :ok
    else
      render json: { errors: @breed_updater.breed.errors }, status: 422
    end
  end
  
  def destroy
    @breed_deleter = BreedDeleter.new(@breed)
    if @breed_deleter.destroy
      head :ok
    else
      render status: 500, json: { message: 'Could not destroy breed' }
    end
  end
  
  private
  
  def set_breed
    @breed = Breed.includes(:tags).find(params[:id])
  end
  
  def breed_params
    params.require(:breed).permit(:name)
  end
end