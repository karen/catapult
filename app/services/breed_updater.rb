class BreedUpdater
  attr_accessor :breed, :breed_params, :tags_params
  attr_reader :breed
  
  def initialize(breed, breed_params=nil, tags_params=nil)
    self.breed = breed
    self.breed_params = breed_params
    self.tags_params = tags_params
  end
  
  def save
    ActiveRecord::Base.transaction do
      if breed_params
        breed.update(breed_params)
      end
      
      if tags_params
        update_tags
      else
        remove_all_tags
      end
    end
  end
  
  private
    
  def update_tags
    return if breed.tags.pluck(:title) == tags_params
    remove_old_tags
    add_new_tags
  end
  
  def add_new_tags
    return unless tags_params
    tags_params.each do |tag|
      next if breed.tags.pluck(:title).include?(tag)
      tag = Tag.find_or_create_by(title: tag)
      breed.tags << tag
    end
  end
  
  def remove_old_tags
    remove_all_tags and return unless tags_params
    breed.tags.pluck(:title).each do |tag|
      next if tags_params.include?(tag)
      tag = Tag.find_by(title: tag)
      next unless tag
      breed.breed_tags.where(tag_id: tag.id).destroy_all
    end
  end
  
  def remove_all_tags
    breed.tags.clear
  end
end