class BreedCreator
  attr_accessor :breed, :breed_params, :tags_params
  attr_reader :breed
  
  def initialize(breed_params, tags_params=nil)
    self.breed_params = breed_params  
    self.tags_params = tags_params
    
    @breed = Breed.new(breed_params)
  end
  
  def save
    ActiveRecord::Base.transaction do
      if @breed.save
        @breed.tags << find_or_create_tags
      end
    end
  end
  
  private
  
  def find_or_create_tags
    tags = []
    return tags unless tags_params
    tags_params.each do |tag|
      tags << Tag.find_or_create_by(title: tag)
    end
    tags
  end
end