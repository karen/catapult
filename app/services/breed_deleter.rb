class BreedDeleter
  attr_accessor :breed
  
  def initialize(breed)
    self.breed = breed
  end
  
  def destroy
    ActiveRecord::Base.transaction do
      tags = breed.tags
      breed.delete
      destroy_tags(tags)
    end
  end
  
  private
  
  def destroy_tags(tags)
    tags.each do |tag|
      if orphan?(tag)
        tag.destroy
      end
    end
  end
  
  def orphan?(tag)
    tag.breeds.none?
  end
end