class TagSerializer < ActiveModel::Serializer
  attributes \
    :id,
    :title, 
    :breed_count,
    :breed_ids
  
  def breed_count
    object.breeds.count
  end
  
  def breed_ids
    object.breed_ids
  end
end
