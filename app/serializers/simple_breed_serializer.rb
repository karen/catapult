class SimpleBreedSerializer < ActiveModel::Serializer
  attributes :name, :tags
  
  def tags
    object.tags.pluck(:title)
  end
end