class BreedSerializer < ActiveModel::Serializer
  attributes :id, :name, :tag_count, :tag_ids
  
  def tag_ids
    object.tag_ids
  end
  
  def tag_count
    object.tags.count
  end
end
