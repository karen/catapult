class SimpleTagSerializer < ActiveModel::Serializer
  attributes :title, :breeds
  
  def breeds
    object.breeds.pluck(:name)
  end
end